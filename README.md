
# News VIPER

A swift UIKit app that gives you a list of news using VIPER architecture. This project was developed as a part of Test Assessment for Bank Mandiri.



## Usage

- Open the project in Xcode.
- Build and run the app on a simulator or device.
- The app will display list of Categories that is available.
- Tap on the Category to view the list of Sources in the Category.
- Use the search bar to filter the Sources by keyword.
- Tap on the Source to view the list of Article in the Source.
- Use the search bar to filter the Articles by keyword.
- Tap on the Article to view it on a Web View.

## Acknowledgements

- [NewsAPI](https://newsapi.org)

