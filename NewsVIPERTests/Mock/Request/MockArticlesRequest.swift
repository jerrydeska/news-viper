//
//  MockArticlesRequest.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

extension ArticlesRequest {
    static func mockRequest() -> ArticlesRequest {
        return .init(sources: "Mock Source")
    }
}
