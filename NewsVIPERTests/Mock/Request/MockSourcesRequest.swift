//
//  MockSourcesRequest.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

extension SourcesRequest {
    static func mockRequest() -> SourcesRequest {
        return .init(category: "Dummy Category", language: nil, country: nil)
    }
}
