//
//  MockAPIRepo.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import RxSwift
@testable import NewsVIPER

class MockAPIRepo: APIRepo {

    var invokedGetSources = false
    var invokedGetSourcesCount = 0
    var invokedGetSourcesParameters: (param: SourcesRequest, Void)?
    var invokedGetSourcesParametersList = [(param: SourcesRequest, Void)]()
    var stubbedGetSourcesResult: Observable<SourcesResponse>!

    func getSources(param: SourcesRequest) -> Observable<SourcesResponse> {
        invokedGetSources = true
        invokedGetSourcesCount += 1
        invokedGetSourcesParameters = (param, ())
        invokedGetSourcesParametersList.append((param, ()))
        return stubbedGetSourcesResult
    }

    var invokedGetArticles = false
    var invokedGetArticlesCount = 0
    var invokedGetArticlesParameters: (param: ArticlesRequest, Void)?
    var invokedGetArticlesParametersList = [(param: ArticlesRequest, Void)]()
    var stubbedGetArticlesResult: Observable<ArticlesResponse>!

    func getArticles(param: ArticlesRequest) -> Observable<ArticlesResponse> {
        invokedGetArticles = true
        invokedGetArticlesCount += 1
        invokedGetArticlesParameters = (param, ())
        invokedGetArticlesParametersList.append((param, ()))
        return stubbedGetArticlesResult
    }
}
