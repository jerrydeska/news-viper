//
//  MockCustomError.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

extension CustomError {
    static func mockError() -> CustomError {
        return .init(status: "error", code: "Mock Code", message: "Mock Message")
    }
}
