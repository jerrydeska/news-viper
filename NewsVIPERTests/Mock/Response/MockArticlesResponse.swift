//
//  MockArticlesResponse.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

extension ArticlesResponse {
    static func mockHasData() -> ArticlesResponse {
        return .init(
            status: "ok",
            totalResults: 5,
            articles: [mockArticlesData]
        )
    }

    static var mockArticlesData: ArticlesData {
        return .init(
            source: mockArticleSource,
            author: "Mock Author",
            title: "Mock Title",
            description: "Mock Description",
            url: "https://www.example.com",
            urlToImage: "https://picsum.photos/300/150",
            publishedAt: "Mock Date",
            content: "1970-1-1T00:00:00Z"
        )
    }

    static var mockArticleSource: ArticleSource {
        return .init(id: "Mock ID", name: "Mock Name")
    }
}
