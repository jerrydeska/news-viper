//
//  MockSourcesResponse.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

extension SourcesResponse {
    static func mockHasData() -> SourcesResponse {
        return .init(
            status: "ok",
            sources: [mocksSourceData]
        )
    }

    static var mocksSourceData: SourceData {
        return .init(
            id: "Mock ID",
            name: "Mock Name",
            description: "Mock Description",
            url: "https://www.example.com",
            category: "Mock Category",
            language: "Mock Language",
            country: "Mock Country"
        )
    }
}
