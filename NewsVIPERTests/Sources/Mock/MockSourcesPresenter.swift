//
//  MockSourcesPresenter.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import RxRelay
@testable import NewsVIPER

class MockSourcesPresenter: SourcesPresenterProtocol {

    var invokedViewSetter = false
    var invokedViewSetterCount = 0
    var invokedView: SourcesViewProtocol?
    var invokedViewList = [SourcesViewProtocol?]()
    var invokedViewGetter = false
    var invokedViewGetterCount = 0
    var stubbedView: SourcesViewProtocol!

    var view: SourcesViewProtocol? {
        set {
            invokedViewSetter = true
            invokedViewSetterCount += 1
            invokedView = newValue
            invokedViewList.append(newValue)
        }
        get {
            invokedViewGetter = true
            invokedViewGetterCount += 1
            return stubbedView
        }
    }

    var invokedInteractorSetter = false
    var invokedInteractorSetterCount = 0
    var invokedInteractor: SourcesInteractorProtocol?
    var invokedInteractorList = [SourcesInteractorProtocol?]()
    var invokedInteractorGetter = false
    var invokedInteractorGetterCount = 0
    var stubbedInteractor: SourcesInteractorProtocol!

    var interactor: SourcesInteractorProtocol? {
        set {
            invokedInteractorSetter = true
            invokedInteractorSetterCount += 1
            invokedInteractor = newValue
            invokedInteractorList.append(newValue)
        }
        get {
            invokedInteractorGetter = true
            invokedInteractorGetterCount += 1
            return stubbedInteractor
        }
    }

    var invokedRouterSetter = false
    var invokedRouterSetterCount = 0
    var invokedRouter: SourcesRouterProtocol?
    var invokedRouterList = [SourcesRouterProtocol?]()
    var invokedRouterGetter = false
    var invokedRouterGetterCount = 0
    var stubbedRouter: SourcesRouterProtocol!

    var router: SourcesRouterProtocol? {
        set {
            invokedRouterSetter = true
            invokedRouterSetterCount += 1
            invokedRouter = newValue
            invokedRouterList.append(newValue)
        }
        get {
            invokedRouterGetter = true
            invokedRouterGetterCount += 1
            return stubbedRouter
        }
    }

    var invokedSectionSetter = false
    var invokedSectionSetterCount = 0
    var invokedSection: BehaviorRelay<[SourceSection]>?
    var invokedSectionList = [BehaviorRelay<[SourceSection]>]()
    var invokedSectionGetter = false
    var invokedSectionGetterCount = 0
    var stubbedSection: BehaviorRelay<[SourceSection]>! = .init(value: [])

    var section: BehaviorRelay<[SourceSection]> {
        set {
            invokedSectionSetter = true
            invokedSectionSetterCount += 1
            invokedSection = newValue
            invokedSectionList.append(newValue)
        }
        get {
            invokedSectionGetter = true
            invokedSectionGetterCount += 1
            return stubbedSection
        }
    }

    var invokedErrorSetter = false
    var invokedErrorSetterCount = 0
    var invokedError: BehaviorRelay<CustomError?>?
    var invokedErrorList = [BehaviorRelay<CustomError?>]()
    var invokedErrorGetter = false
    var invokedErrorGetterCount = 0
    var stubbedError: BehaviorRelay<CustomError?>! = .init(value: nil)

    var error: BehaviorRelay<CustomError?> {
        set {
            invokedErrorSetter = true
            invokedErrorSetterCount += 1
            invokedError = newValue
            invokedErrorList.append(newValue)
        }
        get {
            invokedErrorGetter = true
            invokedErrorGetterCount += 1
            return stubbedError
        }
    }

    var invokedViewDidLoad = false
    var invokedViewDidLoadCount = 0

    func viewDidLoad() {
        invokedViewDidLoad = true
        invokedViewDidLoadCount += 1
    }

    var invokedGetSources = false
    var invokedGetSourcesCount = 0
    var invokedGetSourcesParameters: (request: SourcesRequest, Void)?
    var invokedGetSourcesParametersList = [(request: SourcesRequest, Void)]()

    func getSources(request: SourcesRequest) {
        invokedGetSources = true
        invokedGetSourcesCount += 1
        invokedGetSourcesParameters = (request, ())
        invokedGetSourcesParametersList.append((request, ()))
    }

    var invokedSearchSources = false
    var invokedSearchSourcesCount = 0
    var invokedSearchSourcesParameters: (query: String, Void)?
    var invokedSearchSourcesParametersList = [(query: String, Void)]()

    func searchSources(query: String) {
        invokedSearchSources = true
        invokedSearchSourcesCount += 1
        invokedSearchSourcesParameters = (query, ())
        invokedSearchSourcesParametersList.append((query, ()))
    }

    var invokedNavigateToArticles = false
    var invokedNavigateToArticlesCount = 0
    var invokedNavigateToArticlesParameters: (view: SourcesViewProtocol, source: String, sourceID: String)?
    var invokedNavigateToArticlesParametersList = [(view: SourcesViewProtocol, source: String, sourceID: String)]()

    func navigateToArticles(from view: SourcesViewProtocol, source: String, sourceID: String) {
        invokedNavigateToArticles = true
        invokedNavigateToArticlesCount += 1
        invokedNavigateToArticlesParameters = (view, source, sourceID)
        invokedNavigateToArticlesParametersList.append((view, source, sourceID))
    }
}
