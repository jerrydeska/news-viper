//
//  MockSourcesInteractor.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

class MockSourcesInteractor: SourcesInteractorProtocol {

    var invokedPresenterSetter = false
    var invokedPresenterSetterCount = 0
    var invokedPresenter: SourcesPresenterProtocol?
    var invokedPresenterList = [SourcesPresenterProtocol?]()
    var invokedPresenterGetter = false
    var invokedPresenterGetterCount = 0
    var stubbedPresenter: SourcesPresenterProtocol!

    var presenter: SourcesPresenterProtocol? {
        set {
            invokedPresenterSetter = true
            invokedPresenterSetterCount += 1
            invokedPresenter = newValue
            invokedPresenterList.append(newValue)
        }
        get {
            invokedPresenterGetter = true
            invokedPresenterGetterCount += 1
            return stubbedPresenter
        }
    }

    var invokedTableItemSetter = false
    var invokedTableItemSetterCount = 0
    var invokedTableItem: [SourceItem]?
    var invokedTableItemList = [[SourceItem]]()
    var invokedTableItemGetter = false
    var invokedTableItemGetterCount = 0
    var stubbedTableItem: [SourceItem]! = []

    var tableItem: [SourceItem] {
        set {
            invokedTableItemSetter = true
            invokedTableItemSetterCount += 1
            invokedTableItem = newValue
            invokedTableItemList.append(newValue)
        }
        get {
            invokedTableItemGetter = true
            invokedTableItemGetterCount += 1
            return stubbedTableItem
        }
    }

    var invokedGetSources = false
    var invokedGetSourcesCount = 0
    var invokedGetSourcesParameters: (request: SourcesRequest, Void)?
    var invokedGetSourcesParametersList = [(request: SourcesRequest, Void)]()

    func getSources(request: SourcesRequest) {
        invokedGetSources = true
        invokedGetSourcesCount += 1
        invokedGetSourcesParameters = (request, ())
        invokedGetSourcesParametersList.append((request, ()))
    }

    var invokedSearchSources = false
    var invokedSearchSourcesCount = 0
    var invokedSearchSourcesParameters: (query: String, Void)?
    var invokedSearchSourcesParametersList = [(query: String, Void)]()

    func searchSources(query: String) {
        invokedSearchSources = true
        invokedSearchSourcesCount += 1
        invokedSearchSourcesParameters = (query, ())
        invokedSearchSourcesParametersList.append((query, ()))
    }
}
