//
//  MockSourcesRouter.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import UIKit
@testable import NewsVIPER

class MockSourcesRouter: SourcesRouterProtocol {
    static func createModule(category: String) -> UIViewController {
        return UIViewController()
    }

    var invokedNavigateToArticles = false
    var invokedNavigateToArticlesCount = 0
    var invokedNavigateToArticlesParameters: (view: SourcesViewProtocol, source: String, sourceID: String)?
    var invokedNavigateToArticlesParametersList = [(view: SourcesViewProtocol, source: String, sourceID: String)]()

    func navigateToArticles(from view: SourcesViewProtocol, source: String, sourceID: String) {
        invokedNavigateToArticles = true
        invokedNavigateToArticlesCount += 1
        invokedNavigateToArticlesParameters = (view, source, sourceID)
        invokedNavigateToArticlesParametersList.append((view, source, sourceID))
    }
}
