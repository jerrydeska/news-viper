//
//  SourcesInteractorTests.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import XCTest
import RxSwift
@testable import NewsVIPER

class SourcesInteractorTests: XCTestCase {
    private var sourcesPresenter: MockSourcesPresenter!
    private var apiRepo: MockAPIRepo!
    private var sut: SourcesInteractorProtocol!
    private var disposeBag: DisposeBag!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sourcesPresenter = MockSourcesPresenter()
        apiRepo = MockAPIRepo()
        sut = SourcesInteractor(apiRepo: apiRepo)
        disposeBag = DisposeBag()

        sut.presenter = sourcesPresenter
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        sourcesPresenter = nil
        apiRepo = nil
        sut = nil
        disposeBag = nil
    }

    func test_getSources_whenSuccess() {
        // Given
        let result = SourcesResponse.mockHasData()
        let resultItems = result.sources!.map{ SourceItem.dataItem($0.mappedResponse()) }
        let resultSection = [SourceSection.init(model: "", items: resultItems)]

        let expectation = expectation(description: "`Get Sources` Success")
        apiRepo.stubbedGetSourcesResult = .just(result)

        // When
        sut.getSources(request: SourcesRequest.mockRequest())

        // Then
        sourcesPresenter.stubbedSection
            .subscribe(onNext: {
                XCTAssertEqual($0, resultSection)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }

    func test_getSources_whenError() {
        // Given
        let result = CustomError.mockError()
        let expectation = expectation(description: "`Get Sources` Failed")
        apiRepo.stubbedGetSourcesResult = .error(result)

        // When
        sut.getSources(request: SourcesRequest.mockRequest())

        // Then
        sourcesPresenter.stubbedError
            .subscribe(onNext: {
                XCTAssertEqual($0, result)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }

    func test_searchSources_whenQueryIsNotEmpty() {
        // Given
        let result = SourcesResponse.mockHasData()
        let resultItems = result.sources!.map{ SourceItem.dataItem($0.mappedResponse()) }
        let resultSection = [SourceSection.init(model: "", items: resultItems)]

        let expectation = expectation(description: "`Search Sources` Filtered")
        sut.tableItem = resultItems

        // When
        sut.searchSources(query: "Mock")

        // Then
        sourcesPresenter.stubbedSection
            .subscribe(onNext: {
                XCTAssertEqual($0, resultSection)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }

    func test_searchSources_whenQueryIsEmpty() {
        // Given
        let result = SourcesResponse.mockHasData()
        let resultItems = result.sources!.map{ SourceItem.dataItem($0.mappedResponse()) }
        let resultSection = [SourceSection.init(model: "", items: resultItems)]

        let expectation = expectation(description: "`Search Sources` Not Filtered")
        sut.tableItem = resultItems

        // When
        sut.searchSources(query: "")

        // Then
        sourcesPresenter.stubbedSection
            .subscribe(onNext: {
                XCTAssertEqual($0, resultSection)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }

    func test_searchSources_whenQueryIsNotEmpty_andItemHasSkeleton() {
        // Given
        let resultItems = [SourceItem.skeletonItem]
        let resultSection = [SourceSection.init(model: "", items: [])]

        let expectation = expectation(description: "`Search Sources` Not Filtered")
        sut.tableItem = resultItems

        // When
        sut.searchSources(query: "Mock")

        // Then
        sourcesPresenter.stubbedSection
            .subscribe(onNext: {
                XCTAssertEqual($0, resultSection)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }
}
