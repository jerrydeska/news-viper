//
//  SourcesPresenterTests.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import XCTest
@testable import NewsVIPER

class SourcesPresenterTests: XCTestCase {
    private var sourcesView: MockSourcesView!
    private var sourcesInteractor: MockSourcesInteractor!
    private var sourcesRouter: MockSourcesRouter!
    private var sut: SourcesPresenterProtocol!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sourcesView = MockSourcesView()
        sourcesInteractor = MockSourcesInteractor()
        sourcesRouter = MockSourcesRouter()
        sut = SourcesPresenter()

        sut.view = sourcesView
        sut.interactor = sourcesInteractor
        sut.router = sourcesRouter
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        sourcesView = nil
        sourcesInteractor = nil
        sourcesRouter = nil
        sut = nil
    }

    func test_viewDidLoad() {
        sut.viewDidLoad()
        XCTAssertTrue(sourcesView.invokedSetupUI)
        XCTAssertTrue(sourcesView.invokedSetupConstraint)
        XCTAssertTrue(sourcesView.invokedBindReactive)
    }

    func test_getSources() {
        sut.getSources(request: .init(category: "Dummy Category", language: nil, country: nil))
        XCTAssertTrue(sourcesInteractor.invokedGetSources)
    }

    func test_searchSources() {
        sut.searchSources(query: "Dummy Query")
        XCTAssertTrue(sourcesInteractor.invokedSearchSources)
    }

    func test_navigateToArticle() {
        sut.navigateToArticles(from: sourcesView, source: "Dummy Source", sourceID: "dummy-source")
        XCTAssertTrue(sourcesRouter.invokedNavigateToArticles)
    }
}
