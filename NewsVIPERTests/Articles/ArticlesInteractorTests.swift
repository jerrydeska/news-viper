//
//  ArticlesInteractorTests.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import XCTest
import RxSwift
@testable import NewsVIPER

class ArticlesInteractorTests: XCTestCase {
    private var articlesPresenter: MockArticlesPresenter!
    private var apiRepo: MockAPIRepo!
    private var sut: ArticlesInteractorProtocol!
    private var disposeBag: DisposeBag!
    private var request: ArticlesRequest!

    override func setUpWithError() throws {
        try super.setUpWithError()
        articlesPresenter = MockArticlesPresenter()
        apiRepo = MockAPIRepo()
        request = .mockRequest()
        sut = ArticlesInteractor(
            request: request,
            apiRepo: apiRepo
        )
        disposeBag = DisposeBag()

        sut.presenter = articlesPresenter
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        articlesPresenter = nil
        apiRepo = nil
        sut = nil
        disposeBag = nil
    }

    func test_getArticles_whenSuccess_andQueryIsNotEmpty() {
        // Given
        let result = ArticlesResponse.mockHasData()
        let resultItems = result.articles!.map { ArticleItem.dataItem($0.mappedResponse()) }
        let resultSection = [ArticleSection.init(model: "", items: resultItems)]

        let expectation = expectation(description: "`Get Articles` Success")
        apiRepo.stubbedGetArticlesResult = .just(result)

        // When
        sut.getArticles("Mock", previousItem: [])

        // Then
        articlesPresenter.stubbedSection
            .subscribe(onNext: {
                XCTAssertEqual($0, resultSection)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }

    func test_getArticles_whenSuccess_andQueryIsEmpty() {
        // Given
        let result = ArticlesResponse.mockHasData()
        let resultItems = result.articles!.map { ArticleItem.dataItem($0.mappedResponse()) }
        let resultSection = [ArticleSection.init(model: "", items: resultItems)]

        let expectation = expectation(description: "`Get Articles` Success")
        apiRepo.stubbedGetArticlesResult = .just(result)

        // When
        sut.getArticles("", previousItem: [])

        // Then
        articlesPresenter.stubbedSection
            .subscribe(onNext: {
                XCTAssertEqual($0, resultSection)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }

    func test_getArticles_whenError() {
        // Given
        let result = CustomError.mockError()
        let expectation = expectation(description: "`Get Articles` Failed")
        apiRepo.stubbedGetArticlesResult = .error(result)

        // When
        sut.getArticles(nil, previousItem: [])

        // Then
        articlesPresenter.stubbedError
            .subscribe(onNext: {
                XCTAssertEqual($0, result)
                expectation.fulfill()
            }).disposed(by: disposeBag)

        wait(for: [expectation], timeout: 0.1)
    }
}
