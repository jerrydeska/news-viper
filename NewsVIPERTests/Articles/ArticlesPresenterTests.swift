//
//  ArticlesPresenterTests.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import XCTest
@testable import NewsVIPER

class ArticlesPresenterTests: XCTestCase {
    private var articlesView: MockArticlesView!
    private var articlesInteractor: MockArticlesInteractor!
    private var articlesRouter: MockArticlesRouter!
    private var sut: ArticlesPresenterProtocol!

    override func setUpWithError() throws {
        try super.setUpWithError()
        articlesView = MockArticlesView()
        articlesInteractor = MockArticlesInteractor()
        articlesRouter = MockArticlesRouter()
        sut = ArticlesPresenter()

        sut.view = articlesView
        sut.interactor = articlesInteractor
        sut.router = articlesRouter
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        articlesView = nil
        articlesInteractor = nil
        articlesRouter = nil
        sut = nil
    }

    func test_viewDidLoad() {
        sut.viewDidLoad()
        XCTAssertTrue(articlesView.invokedSetupUI)
        XCTAssertTrue(articlesView.invokedSetupConstraint)
        XCTAssertTrue(articlesView.invokedBindReactive)
    }

    func test_searchArticles() {
        sut.searchArticles("Mock Query")
        XCTAssertTrue(articlesInteractor.invokedGetArticles)
    }

    func test_reloadArticles() {
        sut.reloadArticles()
        XCTAssertTrue(articlesInteractor.invokedGetArticles)
    }

    func test_loadMoreArticles() {
        let result = ArticlesResponse.mockHasData()
        let resultItems = result.articles!.map{ ArticleItem.dataItem($0.mappedResponse()) }

        sut.totalResults = result.totalResults!
        sut.section = .init(value: [.init(model: "", items: resultItems)])

        sut.loadMoreArticles(pos: 0)

        XCTAssertTrue(articlesInteractor.invokedGetArticles)
        XCTAssertTrue(articlesView.invokedShowFooterLoading)
    }

    func test_navigateToWebView() {
        sut.navigateToWebView(from: articlesView, url: URL(string: "https://www.example.com")!)
        XCTAssertTrue(articlesRouter.invokedNavigateToWebView)
    }
}
