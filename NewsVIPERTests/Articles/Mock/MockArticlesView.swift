//
//  MockArticlesView.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

class MockArticlesView: ArticlesViewProtocol {

    var invokedSetupUI = false
    var invokedSetupUICount = 0

    func setupUI() {
        invokedSetupUI = true
        invokedSetupUICount += 1
    }

    var invokedSetupConstraint = false
    var invokedSetupConstraintCount = 0

    func setupConstraint() {
        invokedSetupConstraint = true
        invokedSetupConstraintCount += 1
    }

    var invokedBindReactive = false
    var invokedBindReactiveCount = 0

    func bindReactive() {
        invokedBindReactive = true
        invokedBindReactiveCount += 1
    }

    var invokedShowFooterLoading = false
    var invokedShowFooterLoadingCount = 0

    func showFooterLoading() {
        invokedShowFooterLoading = true
        invokedShowFooterLoadingCount += 1
    }
}
