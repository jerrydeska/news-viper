//
//  MockArticlesInteractor.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

@testable import NewsVIPER

class MockArticlesInteractor: ArticlesInteractorProtocol {

    var invokedPresenterSetter = false
    var invokedPresenterSetterCount = 0
    var invokedPresenter: ArticlesPresenterProtocol?
    var invokedPresenterList = [ArticlesPresenterProtocol?]()
    var invokedPresenterGetter = false
    var invokedPresenterGetterCount = 0
    var stubbedPresenter: ArticlesPresenterProtocol!

    var presenter: ArticlesPresenterProtocol? {
        set {
            invokedPresenterSetter = true
            invokedPresenterSetterCount += 1
            invokedPresenter = newValue
            invokedPresenterList.append(newValue)
        }
        get {
            invokedPresenterGetter = true
            invokedPresenterGetterCount += 1
            return stubbedPresenter
        }
    }

    var invokedRequestSetter = false
    var invokedRequestSetterCount = 0
    var invokedRequest: ArticlesRequest?
    var invokedRequestList = [ArticlesRequest]()
    var invokedRequestGetter = false
    var invokedRequestGetterCount = 0
    var stubbedRequest: ArticlesRequest! = .mockRequest()

    var request: ArticlesRequest {
        set {
            invokedRequestSetter = true
            invokedRequestSetterCount += 1
            invokedRequest = newValue
            invokedRequestList.append(newValue)
        }
        get {
            invokedRequestGetter = true
            invokedRequestGetterCount += 1
            return stubbedRequest
        }
    }

    var invokedGetArticles = false
    var invokedGetArticlesCount = 0
    var invokedGetArticlesParameters: (query: String?, previousItem: [ArticleItem])?
    var invokedGetArticlesParametersList = [(query: String?, previousItem: [ArticleItem])]()

    func getArticles(_ query: String?, previousItem: [ArticleItem]) {
        invokedGetArticles = true
        invokedGetArticlesCount += 1
        invokedGetArticlesParameters = (query, previousItem)
        invokedGetArticlesParametersList.append((query, previousItem))
    }
}
