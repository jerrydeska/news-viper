//
//  MockArticlesRouter.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import UIKit
@testable import NewsVIPER

class MockArticlesRouter: ArticlesRouterProtocol {
    static func createModule(source: String, sourceID: String) -> UIViewController {
        return UIViewController()
    }

    var invokedNavigateToWebView = false
    var invokedNavigateToWebViewCount = 0
    var invokedNavigateToWebViewParameters: (view: ArticlesViewProtocol, url: URL)?
    var invokedNavigateToWebViewParametersList = [(view: ArticlesViewProtocol, url: URL)]()

    func navigateToWebView(from view: ArticlesViewProtocol, url: URL) {
        invokedNavigateToWebView = true
        invokedNavigateToWebViewCount += 1
        invokedNavigateToWebViewParameters = (view, url)
        invokedNavigateToWebViewParametersList.append((view, url))
    }
}
