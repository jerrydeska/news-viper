//
//  MockArticlesPresenter.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import RxRelay
@testable import NewsVIPER

class MockArticlesPresenter: ArticlesPresenterProtocol {

    var invokedViewSetter = false
    var invokedViewSetterCount = 0
    var invokedView: ArticlesViewProtocol?
    var invokedViewList = [ArticlesViewProtocol?]()
    var invokedViewGetter = false
    var invokedViewGetterCount = 0
    var stubbedView: ArticlesViewProtocol!

    var view: ArticlesViewProtocol? {
        set {
            invokedViewSetter = true
            invokedViewSetterCount += 1
            invokedView = newValue
            invokedViewList.append(newValue)
        }
        get {
            invokedViewGetter = true
            invokedViewGetterCount += 1
            return stubbedView
        }
    }

    var invokedInteractorSetter = false
    var invokedInteractorSetterCount = 0
    var invokedInteractor: ArticlesInteractorProtocol?
    var invokedInteractorList = [ArticlesInteractorProtocol?]()
    var invokedInteractorGetter = false
    var invokedInteractorGetterCount = 0
    var stubbedInteractor: ArticlesInteractorProtocol!

    var interactor: ArticlesInteractorProtocol? {
        set {
            invokedInteractorSetter = true
            invokedInteractorSetterCount += 1
            invokedInteractor = newValue
            invokedInteractorList.append(newValue)
        }
        get {
            invokedInteractorGetter = true
            invokedInteractorGetterCount += 1
            return stubbedInteractor
        }
    }

    var invokedRouterSetter = false
    var invokedRouterSetterCount = 0
    var invokedRouter: ArticlesRouterProtocol?
    var invokedRouterList = [ArticlesRouterProtocol?]()
    var invokedRouterGetter = false
    var invokedRouterGetterCount = 0
    var stubbedRouter: ArticlesRouterProtocol!

    var router: ArticlesRouterProtocol? {
        set {
            invokedRouterSetter = true
            invokedRouterSetterCount += 1
            invokedRouter = newValue
            invokedRouterList.append(newValue)
        }
        get {
            invokedRouterGetter = true
            invokedRouterGetterCount += 1
            return stubbedRouter
        }
    }

    var invokedSectionSetter = false
    var invokedSectionSetterCount = 0
    var invokedSection: BehaviorRelay<[ArticleSection]>?
    var invokedSectionList = [BehaviorRelay<[ArticleSection]>]()
    var invokedSectionGetter = false
    var invokedSectionGetterCount = 0
    var stubbedSection: BehaviorRelay<[ArticleSection]>! = .init(value: [])

    var section: BehaviorRelay<[ArticleSection]> {
        set {
            invokedSectionSetter = true
            invokedSectionSetterCount += 1
            invokedSection = newValue
            invokedSectionList.append(newValue)
        }
        get {
            invokedSectionGetter = true
            invokedSectionGetterCount += 1
            return stubbedSection
        }
    }

    var invokedErrorSetter = false
    var invokedErrorSetterCount = 0
    var invokedError: BehaviorRelay<CustomError?>?
    var invokedErrorList = [BehaviorRelay<CustomError?>]()
    var invokedErrorGetter = false
    var invokedErrorGetterCount = 0
    var stubbedError: BehaviorRelay<CustomError?>! = .init(value: nil)

    var error: BehaviorRelay<CustomError?> {
        set {
            invokedErrorSetter = true
            invokedErrorSetterCount += 1
            invokedError = newValue
            invokedErrorList.append(newValue)
        }
        get {
            invokedErrorGetter = true
            invokedErrorGetterCount += 1
            return stubbedError
        }
    }

    var invokedTotalResultsSetter = false
    var invokedTotalResultsSetterCount = 0
    var invokedTotalResults: Int?
    var invokedTotalResultsList = [Int]()
    var invokedTotalResultsGetter = false
    var invokedTotalResultsGetterCount = 0
    var stubbedTotalResults: Int! = 0

    var totalResults: Int {
        set {
            invokedTotalResultsSetter = true
            invokedTotalResultsSetterCount += 1
            invokedTotalResults = newValue
            invokedTotalResultsList.append(newValue)
        }
        get {
            invokedTotalResultsGetter = true
            invokedTotalResultsGetterCount += 1
            return stubbedTotalResults
        }
    }

    var invokedViewDidLoad = false
    var invokedViewDidLoadCount = 0

    func viewDidLoad() {
        invokedViewDidLoad = true
        invokedViewDidLoadCount += 1
    }

    var invokedSearchArticles = false
    var invokedSearchArticlesCount = 0
    var invokedSearchArticlesParameters: (query: String?, Void)?
    var invokedSearchArticlesParametersList = [(query: String?, Void)]()

    func searchArticles(_ query: String?) {
        invokedSearchArticles = true
        invokedSearchArticlesCount += 1
        invokedSearchArticlesParameters = (query, ())
        invokedSearchArticlesParametersList.append((query, ()))
    }

    var invokedReloadArticles = false
    var invokedReloadArticlesCount = 0

    func reloadArticles() {
        invokedReloadArticles = true
        invokedReloadArticlesCount += 1
    }

    var invokedLoadMoreArticles = false
    var invokedLoadMoreArticlesCount = 0
    var invokedLoadMoreArticlesParameters: (pos: Int, Void)?
    var invokedLoadMoreArticlesParametersList = [(pos: Int, Void)]()

    func loadMoreArticles(pos: Int) {
        invokedLoadMoreArticles = true
        invokedLoadMoreArticlesCount += 1
        invokedLoadMoreArticlesParameters = (pos, ())
        invokedLoadMoreArticlesParametersList.append((pos, ()))
    }

    var invokedNavigateToWebView = false
    var invokedNavigateToWebViewCount = 0
    var invokedNavigateToWebViewParameters: (view: ArticlesViewProtocol, url: URL)?
    var invokedNavigateToWebViewParametersList = [(view: ArticlesViewProtocol, url: URL)]()

    func navigateToWebView(from view: ArticlesViewProtocol, url: URL) {
        invokedNavigateToWebView = true
        invokedNavigateToWebViewCount += 1
        invokedNavigateToWebViewParameters = (view, url)
        invokedNavigateToWebViewParametersList.append((view, url))
    }
}
