//
//  CategoriesPresenterTests.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import XCTest
@testable import NewsVIPER

class CategoriesPresenterTests: XCTestCase {
    private var categoriesView: MockCategoriesView!
    private var categoriesRouter: MockCategoriesRouter!
    private var sut: CategoriesPresenterProtocol!

    override func setUpWithError() throws {
        try super.setUpWithError()
        categoriesView = MockCategoriesView()
        categoriesRouter = MockCategoriesRouter()
        sut = CategoriesPresenter()

        sut.view = categoriesView
        sut.router = categoriesRouter
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        categoriesView = nil
        categoriesRouter = nil
        sut = nil
    }

    func test_viewDidLoad() {
        sut.viewDidLoad()
        XCTAssertTrue(categoriesView.invokedSetupUI)
        XCTAssertTrue(categoriesView.invokedSetupConstraint)
        XCTAssertTrue(categoriesView.invokedBindReactive)
    }

    func test_navigateToSources() {
        sut.navigateToSources(from: categoriesView, category: "Dummy Category")
        XCTAssertTrue(categoriesRouter.invokedNavigateToSources)
    }
}
