//
//  MockCategoriesRouter.swift
//  NewsVIPERTests
//
//  Created by Jerry Deska on 16/12/22.
//

import UIKit
@testable import NewsVIPER

class MockCategoriesRouter: CategoriesRouterProtocol {
    static func createModule() -> UINavigationController {
        return UINavigationController()
    }

    var invokedNavigateToSources = false
    var invokedNavigateToSourcesCount = 0
    var invokedNavigateToSourcesParameters: (view: CategoriesViewProtocol, category: String)?
    var invokedNavigateToSourcesParametersList = [(view: CategoriesViewProtocol, category: String)]()

    func navigateToSources(from view: CategoriesViewProtocol, category: String) {
        invokedNavigateToSources = true
        invokedNavigateToSourcesCount += 1
        invokedNavigateToSourcesParameters = (view, category)
        invokedNavigateToSourcesParametersList.append((view, category))
    }
}
