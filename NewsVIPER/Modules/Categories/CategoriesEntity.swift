//
//  CategoriesEntity.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

enum Categories: String {
    case business = "Business"
    case entertainment = "Entertainment"
    case general = "General"
    case health = "Health"
    case science = "Science"
    case sports = "Sports"
    case technology = "Technology"

    var backgroundColor: UIColor {
        switch self {
        case .business:
            return UIColor(named: "businessCategory")!
        case .entertainment:
            return UIColor(named: "entertainmentCategory")!
        case .general:
            return UIColor(named: "generalCategory")!
        case .health:
            return UIColor(named: "healthCategory")!
        case .science:
            return UIColor(named: "scienceCategory")!
        case .sports:
            return UIColor(named: "sportsCategory")!
        case .technology:
            return UIColor(named: "technologyCategory")!
        }
    }
}
