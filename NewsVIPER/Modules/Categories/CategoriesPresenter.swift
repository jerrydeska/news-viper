//
//  CategoriesPresenter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import RxRelay

class CategoriesPresenter: CategoriesPresenterProtocol {
    weak var view: CategoriesViewProtocol?
    var router: CategoriesRouterProtocol?

    var categories: BehaviorRelay<[Categories]> = .init(value: [
        .business, .entertainment, .general, .health, .science, .sports, .technology
    ])

    func viewDidLoad() {
        view?.setupUI()
        view?.setupConstraint()
        view?.bindReactive()
    }

    func navigateToSources(from view: CategoriesViewProtocol, category: String) {
        router?.navigateToSources(from: view, category: category)
    }
}
