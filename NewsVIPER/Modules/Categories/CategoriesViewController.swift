//
//  CategoriesViewController.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class CategoriesViewController: CustomViewController {
    var presenter: CategoriesPresenterProtocol?

    private let categoriesCollectionView: UICollectionView = {
        let collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: .init()
        )
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout.minimumInteritemSpacing = 8
        flowLayout.minimumLineSpacing = 8
        flowLayout.sectionInset = .init(top: 8, left: 16, bottom: 8, right: 16)
        collectionView.collectionViewLayout = flowLayout
        collectionView.backgroundColor = .clear
        collectionView.registerCells(CategoriesCollectionViewCell.self)
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension CategoriesViewController: CategoriesViewProtocol {
    func setupUI() {
        title = "Categories"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.addSubview(categoriesCollectionView)
    }

    func setupConstraint() {
        categoriesCollectionView.snp.makeConstraints {
            $0.edges.equalTo(view.safeAreaLayoutGuide)
        }
    }

    func bindReactive() {
        // Bind data to table view
        presenter?.categories
            .bind(to: categoriesCollectionView.rx.items(
                cellIdentifier: String(describing: CategoriesCollectionViewCell.self),
                cellType: CategoriesCollectionViewCell.self
            )) { (_, item, cell) in
                cell.bind(for: item)
            }.disposed(by: disposeBag)

        // Action selected when cell is clicked
        categoriesCollectionView.rx.modelSelected(Categories.self)
            .bind { [weak self] in
                guard let self = self else { return }
                self.presenter?.navigateToSources(from: self, category: $0.rawValue)
            }.disposed(by: disposeBag)

        // Set delegate to set cell size so collection view will only show two cell per row
        categoriesCollectionView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }
}

extension CategoriesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let width = collectionView.bounds.width
        let cellSize = (width - 42) / 2
        return CGSize(width: cellSize, height: cellSize/2)
    }
}
