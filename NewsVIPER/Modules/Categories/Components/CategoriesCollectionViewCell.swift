//
//  CategoriesCollectionViewCell.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit
import SnapKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()

    private let categoryLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Placeholder",
                textColor: .systemBackground,
                textFont: .systemFont(ofSize: 16, weight: .bold),
                alignment: .center
            )
        label.attributedText = attributedText
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        setupConstraint()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(for category: Categories) {
        containerView.backgroundColor = category.backgroundColor
        categoryLabel.attributedText = categoryLabel.attributedText?.changeText(category.rawValue)
    }
}

extension CategoriesCollectionViewCell {
    private func setupUI() {
        containerView.addSubview(categoryLabel)
        contentView.addSubview(containerView)
    }

    private func setupConstraint() {
        categoryLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
