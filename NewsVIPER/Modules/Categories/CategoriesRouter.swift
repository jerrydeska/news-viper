//
//  CategoriesRouter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

class CategoriesRouter: CategoriesRouterProtocol {
    static func createModule() -> UINavigationController {
        let presenter = CategoriesPresenter()
        let viewController = CategoriesViewController()
        viewController.presenter = presenter
        viewController.presenter?.view = viewController
        viewController.presenter?.router = CategoriesRouter()

        let navigationController = UINavigationController(rootViewController: viewController)
        return navigationController
    }

    func navigateToSources(from view: CategoriesViewProtocol, category: String) {
        let nextViewController = SourcesRouter.createModule(category: category)
        let currentViewController = view as! CategoriesViewController
        currentViewController.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
