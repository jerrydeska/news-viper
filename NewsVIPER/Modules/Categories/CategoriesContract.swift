//
//  CategoriesContract.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit
import RxRelay

protocol CategoriesViewProtocol: AnyObject {
    func setupUI()
    func setupConstraint()
    func bindReactive()
}

protocol CategoriesPresenterProtocol {
    var view: CategoriesViewProtocol? { get set }
    var router: CategoriesRouterProtocol? { get set }

    var categories: BehaviorRelay<[Categories]> { get set }

    func viewDidLoad()
    func navigateToSources(from view: CategoriesViewProtocol, category: String)
}

protocol CategoriesRouterProtocol {
    static func createModule() -> UINavigationController
    func navigateToSources(from view: CategoriesViewProtocol, category: String)
}
