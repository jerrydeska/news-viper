//
//  SourcesInteractor.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import RxSwift
import RxCocoa

class SourcesInteractor: SourcesInteractorProtocol {
    weak var presenter: SourcesPresenterProtocol?
    var tableItem = [SourceItem]()

    private let apiRepo: APIRepo
    private let disposeBag = DisposeBag()
    

    init(
        apiRepo: APIRepo = APIRepoImplementation()
    ) {
        self.apiRepo = apiRepo
    }

    func getSources(request: SourcesRequest) {
        self.presenter?.error.accept(nil)

        apiRepo.getSources(param: request)
            .subscribe(onNext: { [weak self] in
                if let data = $0.sources, let self = self {
                    self.tableItem = data.map { SourceItem.dataItem($0.mappedResponse()) }
                    self.presenter?.section.accept([.init(model: "", items: self.tableItem)])
                }
            }, onError: { [weak self] in
                self?.presenter?.section.accept([])
                self?.presenter?.error.accept($0 as? CustomError)
            }).disposed(by: disposeBag)
    }

    func searchSources(query: String) {
        guard !query.isEmpty else {
            presenter?.section.accept([.init(model: "", items: tableItem)])
            return
        }

        let searchedItem = tableItem.filter {
            switch $0 {
            case .dataItem(let data):
                return data.title.lowercased().contains(query.lowercased())
            default:
                return false
            }
        }

        presenter?.section.accept([.init(model: "", items: searchedItem)])
    }
}
