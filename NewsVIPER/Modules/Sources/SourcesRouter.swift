//
//  SourcesRouter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import UIKit
import SafariServices

class SourcesRouter: SourcesRouterProtocol {
    static func createModule(category: String) -> UIViewController {
        let presenter = SourcesPresenter()
        let viewController = SourcesViewController(
            request: .init(
                category: category,
                language: nil,
                country: nil
            )
        )
        viewController.presenter = presenter
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = SourcesInteractor()
        viewController.presenter?.router = SourcesRouter()
        viewController.presenter?.interactor?.presenter = presenter

        return viewController
    }

    func navigateToArticles(from view: SourcesViewProtocol, source: String, sourceID: String) {
        let nextViewController = ArticlesRouter.createModule(source: source, sourceID: sourceID)
        let currentViewController = view as! SourcesViewController
        currentViewController.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func navigateToSafariController(from view: SourcesViewProtocol, url: URL) {
        let nextViewController = SFSafariViewController(url: url)
        nextViewController.modalPresentationStyle = .overFullScreen
        let currentViewController = view as! SourcesViewController
        currentViewController.present(nextViewController, animated: true)
    }
}
