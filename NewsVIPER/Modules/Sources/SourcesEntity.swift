//
//  SourcesEntity.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import RxDataSources

// Map response to SourceDataCell
extension SourceData {
    func mappedResponse() -> SourceDataCell {
        let name = self.name ?? "-"
        let language = self.language ?? "-"
        let country = self.country ?? "-"
        let description = self.description ?? "-"
        let url = self.url ?? "-"
        let id = self.id ?? "-"

        return .init(
            id: id,
            title: "\(name) (\(language)-\(country))",
            url: url,
            description: description
        )
    }
}

struct SourceDataCell: Equatable {
    let id: String
    let title: String
    let url: String
    let description: String
}

// Section for RxDataSource
enum SourceItem: Equatable {
    case skeletonItem
    case dataItem(SourceDataCell)

    static func == (lhs: SourceItem, rhs: SourceItem) -> Bool {
        switch (lhs, rhs) {
        case (.skeletonItem, .skeletonItem):
            return true
        case (dataItem(let lData), dataItem(let rData)):
            return lData == rData
        default:
            return false
        }
    }
}

typealias SourceSection = SectionModel<String, SourceItem>
