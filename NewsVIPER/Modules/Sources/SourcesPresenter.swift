//
//  SourcesPresenter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import RxRelay

class SourcesPresenter: SourcesPresenterProtocol {
    weak var view: SourcesViewProtocol?
    var interactor: SourcesInteractorProtocol?
    var router: SourcesRouterProtocol?

    var section: BehaviorRelay<[SourceSection]> = .init(value: [])
    var error: BehaviorRelay<CustomError?> = .init(value: nil)

    func viewDidLoad() {
        view?.setupUI()
        view?.setupConstraint()
        view?.bindReactive()
    }

    func getSources(request: SourcesRequest) {
        section.accept([.init(model: "", items: Array(repeating: .skeletonItem, count: 5))])
        interactor?.getSources(request: request)
    }

    func searchSources(query: String) {
        interactor?.searchSources(query: query)
    }

    func navigateToArticles(from view: SourcesViewProtocol, source: String, sourceID: String) {
        router?.navigateToArticles(from: view, source: source, sourceID: sourceID)
    }
    
    func navigateToSafariController(from view: SourcesViewProtocol, url: URL) {
        router?.navigateToSafariController(from: view, url: url)
    }
}
