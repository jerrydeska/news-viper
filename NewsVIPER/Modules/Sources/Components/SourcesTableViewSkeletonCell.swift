//
//  SourcesTableViewSkeletonCell.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import SnapKit
import SkeletonView

class SourcesTableViewSkeletonCell: UITableViewCell {
    private lazy var titleSkeleton = CustomSkeletonView()
    private lazy var urlSkeleton = CustomSkeletonView()
    private lazy var descriptionSkeleton = CustomSkeletonView()

    private lazy var skeletonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .leading
        return stackView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupUI()
        setupConstraint()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        [titleSkeleton, urlSkeleton, descriptionSkeleton].forEach { $0.showAnimatedGradientSkeleton() }
    }
}

extension SourcesTableViewSkeletonCell {
    private func setupUI() {
        skeletonStackView.addMultipleArrangeSubviews(titleSkeleton, urlSkeleton, descriptionSkeleton)
        skeletonStackView.setCustomSpacing(8, after: urlSkeleton)
        contentView.addSubview(skeletonStackView)

        selectionStyle = .none
        isUserInteractionEnabled = false
    }

    private func setupConstraint() {
        skeletonStackView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(16)
            $0.trailing.bottom.equalToSuperview().inset(16)
        }

        titleSkeleton.snp.makeConstraints {
            $0.height.equalTo(16).priority(.high)
            $0.width.equalToSuperview().multipliedBy(0.5)
        }

        urlSkeleton.snp.makeConstraints {
            $0.height.equalTo(12).priority(.high)
            $0.width.equalToSuperview().multipliedBy(0.4)
        }

        descriptionSkeleton.snp.makeConstraints {
            $0.height.equalTo(36).priority(.high)
            $0.width.equalToSuperview()
        }
    }
}
