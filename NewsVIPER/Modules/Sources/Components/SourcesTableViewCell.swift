//
//  SourcesTableViewCell.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import UIKit
import SnapKit
import RxSwift
import RxGesture

class SourcesTableViewCell: UITableViewCell {
    private var urlAction: ActionTap?
    private let disposeBag = DisposeBag()

    private let titleLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Title Placeholder",
                textColor: .label,
                textFont: .systemFont(ofSize: 16, weight: .bold),
                alignment: .left
            )
        label.attributedText = attributedText
        return label
    }()

    private let urlLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "URL Placeholder",
                textColor: .link,
                textFont: .systemFont(ofSize: 12),
                alignment: .left
            )
        label.attributedText = attributedText
        return label
    }()

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Description Placeholder",
                textColor: .secondaryLabel,
                textFont: .systemFont(ofSize: 12),
                alignment: .left
            )
        label.attributedText = attributedText
        label.numberOfLines = 0
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupUI()
        setupConstraint()
        bindReactive()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(data: SourceDataCell, urlAction: ActionTap?) {
        titleLabel.attributedText = titleLabel.attributedText?.changeText(data.title)
        descriptionLabel.attributedText = descriptionLabel.attributedText?.changeText(data.description)
        urlLabel.attributedText = urlLabel.attributedText?.changeText(data.url)

        self.urlAction = urlAction
    }
}

extension SourcesTableViewCell {
    private func setupUI() {
        contentView.addMultipleSubviews(titleLabel, descriptionLabel, urlLabel)
        selectionStyle = .none
    }

    private func setupConstraint() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(16)
            $0.trailing.equalToSuperview().inset(16)
        }

        urlLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(4)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.lessThanOrEqualToSuperview().inset(16)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(urlLabel.snp.bottom).offset(8)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.bottom.equalToSuperview().inset(16)
        }
    }

    private func bindReactive() {
        urlLabel.rx.tapGesture().when(.recognized)
            .subscribe(onNext: { [weak self] _ in
                self?.urlAction?()
            }).disposed(by: disposeBag)
    }
}
