//
//  SourcesViewController.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SnapKit

class SourcesViewController: CustomViewController {
    var presenter: SourcesPresenterProtocol?
    private let request: SourcesRequest

    private let searchTextField: CustomTextField = {
        let textField = CustomTextField()
        textField.setPlaceholder(text: "Search Sources")
        textField.setSystemImage(image: "magnifyingglass", type: .left)
        return textField
    }()

    private let refreshControl = UIRefreshControl()

    private let sourcesTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.registerCells(
            SourcesTableViewCell.self,
            SourcesTableViewSkeletonCell.self
        )
        return tableView
    }()

    private let negativeView: CustomNegativeView = {
        let negativeView = CustomNegativeView()
        negativeView.isHidden = true
        return negativeView
    }()

    init(request: SourcesRequest) {
        self.request = request
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        presenter?.getSources(request: request)
    }
}

extension SourcesViewController: SourcesViewProtocol {
    func setupUI() {
        let category = request.category ?? ""
        title = "\(category) Sources"

        self.view.addMultipleSubviews(
            searchTextField,
            sourcesTableView,
            negativeView
        )

        sourcesTableView.refreshControl = refreshControl
        navigationItem.largeTitleDisplayMode = .never
    }

    func setupConstraint() {
        searchTextField.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
        }

        sourcesTableView.snp.makeConstraints {
            $0.top.equalTo(searchTextField.snp.bottom).offset(16)
            $0.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide)
        }

        negativeView.snp.makeConstraints {
            $0.top.equalTo(searchTextField).offset(16)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
    }

    func bindReactive() {
        // Create data source for table view
        let dataSource = RxTableViewSectionedReloadDataSource<SourceSection>(
            configureCell: { [weak self] (_, tableView, indexPath, item) in
                switch item {
                case .dataItem(let data):
                    let cell: SourcesTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    cell.bind(data: data) {
                        guard let url = URL(string: data.url),
                              UIApplication.shared.canOpenURL(url),
                              let self = self
                        else { return }

                        self.presenter?.navigateToSafariController(from: self, url: url)
                    }
                    return cell
                case .skeletonItem:
                    let cell: SourcesTableViewSkeletonCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    return cell
                }
            })

        // Bind section with data source
        presenter?.section
            .bind(to: sourcesTableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        // Action when cell is selected
        sourcesTableView.rx.itemSelected
            .subscribe(onNext: { [weak self] in
                guard let self = self,
                      let data = dataSource.sectionModels.first?.items[$0.row]
                else { return }

                switch data {
                case .dataItem(let data):
                    self.presenter?.navigateToArticles(
                        from: self,
                        source: data.title,
                        sourceID: data.id
                    )
                default:
                    break
                }
            }).disposed(by: disposeBag)

        // Set delegate to remove header
        sourcesTableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        // Action when refresh contol is pulled
        refreshControl.rx.controlEvent(.valueChanged)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.presenter?.getSources(request: self.request)
            }).disposed(by: disposeBag)

        // Subscribe to data
        presenter?.section
            .subscribe(onNext: { [weak self] in
                guard let self = self, let items = $0.first?.items else { return }
                // Stop refresh control
                if self.refreshControl.isRefreshing == true {
                    self.refreshControl.endRefreshing()
                }

                // Hide or show tablel view and negative view based on items
                self.sourcesTableView.isHidden = items.isEmpty
                self.negativeView.isHidden = !items.isEmpty

                // Set negative view if items is empty
                if items.isEmpty {
                    self.negativeView.setView(.empty(label: "Sources not found"))
                }
            }).disposed(by: disposeBag)

        // Subscribe to changes in search text field
        searchTextField.rx.text.orEmpty
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .skip(1)
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] in
                self?.presenter?.searchSources(query: $0)
            }).disposed(by: disposeBag)

        // Subscribe to error
        presenter?.error
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.sourcesTableView.isHidden = $0 != nil
                self.negativeView.isHidden = $0 == nil
                self.searchTextField.isEnabled = $0 == nil

                if let errorMessage = $0?.message {
                    self.negativeView.setView(
                        .error(label: errorMessage, action: {
                            self.presenter?.getSources(request: self.request)
                        }))
                }
            }).disposed(by: disposeBag)
        
    }
}

extension SourcesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
}
