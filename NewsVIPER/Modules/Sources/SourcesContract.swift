//
//  SourcesContract.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import UIKit
import RxRelay

protocol SourcesViewProtocol: AnyObject {
    func setupUI()
    func setupConstraint()
    func bindReactive()
}

protocol SourcesInteractorProtocol {
    var presenter: SourcesPresenterProtocol? { get set }
    var tableItem: [SourceItem] { get set }

    func getSources(request: SourcesRequest)
    func searchSources(query: String)
}

protocol SourcesPresenterProtocol: AnyObject {
    var view: SourcesViewProtocol? { get set }
    var interactor: SourcesInteractorProtocol? { get set }
    var router: SourcesRouterProtocol? { get set }

    var section: BehaviorRelay<[SourceSection]> { get set }
    var error: BehaviorRelay<CustomError?> { get set }

    func viewDidLoad()
    func getSources(request: SourcesRequest)
    func searchSources(query: String)
    func navigateToArticles(from view: SourcesViewProtocol, source: String, sourceID: String)
    func navigateToSafariController(from view: SourcesViewProtocol, url: URL)
}

protocol SourcesRouterProtocol {
    static func createModule(category: String) -> UIViewController
    func navigateToArticles(from view: SourcesViewProtocol, source: String, sourceID: String)
    func navigateToSafariController(from view: SourcesViewProtocol, url: URL)
}
