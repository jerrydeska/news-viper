//
//  ArticlesPresenter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import RxRelay

class ArticlesPresenter: ArticlesPresenterProtocol {
    weak var view: ArticlesViewProtocol?
    var interactor: ArticlesInteractorProtocol?
    var router: ArticlesRouterProtocol?

    var section: BehaviorRelay<[ArticleSection]> = .init(value: [])
    var error: BehaviorRelay<CustomError?> = .init(value: nil)
    var totalResults: Int = 0

    func viewDidLoad() {
        view?.setupUI()
        view?.setupConstraint()
        view?.bindReactive()
    }

    func searchArticles(_ query: String? = nil) {
        interactor?.request.resetPage()
        interactor?.getArticles(query, previousItem: [])
        section.accept([.init(model: "", items: Array(repeating: .skeletonItem, count: 5))])
    }

    func reloadArticles() {
        interactor?.request.resetPage()
        interactor?.getArticles(nil, previousItem: [])
        section.accept([.init(model: "", items: Array(repeating: .skeletonItem, count: 5))])
    }

    func loadMoreArticles(pos: Int) {
        if let currentDataCount = section.value.first?.items.count,
           currentDataCount - 1 == pos && currentDataCount < totalResults {
            interactor?.request.loadNextPage()
            interactor?.getArticles(nil, previousItem: section.value.first?.items ?? [])
            view?.showFooterLoading()
        }
    }

    func navigateToWebView(from view: ArticlesViewProtocol, url: URL) {
        router?.navigateToWebView(from: view, url: url)
    }
}
