//
//  ArticlesViewController.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SnapKit

class ArticlesViewController: CustomViewController {
    var presenter: ArticlesPresenterProtocol?
    private let source: String

    private let searchTextField: CustomTextField = {
        let textField = CustomTextField()
        textField.setPlaceholder(text: "Search Articles")
        textField.setSystemImage(image: "magnifyingglass", type: .left)
        return textField
    }()

    private let refreshControl = UIRefreshControl()

    private let articlesTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.registerCells(
            ArticlesTableViewCell.self,
            ArticlesTableViewSkeletonCell.self
        )
        return tableView
    }()

    init(source: String) {
        self.source = source
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private let negativeView: CustomNegativeView = {
        let negativeView = CustomNegativeView()
        negativeView.isHidden = true
        return negativeView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension ArticlesViewController: ArticlesViewProtocol {
    func setupUI() {
        title = source

        self.view.addMultipleSubviews(
            searchTextField,
            articlesTableView,
            negativeView
        )

        articlesTableView.refreshControl = refreshControl
        navigationItem.largeTitleDisplayMode = .never
    }

    func setupConstraint() {
        searchTextField.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
        }

        articlesTableView.snp.makeConstraints {
            $0.top.equalTo(searchTextField.snp.bottom).offset(16)
            $0.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide)
        }

        negativeView.snp.makeConstraints {
            $0.top.equalTo(searchTextField).offset(16)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
    }

    func bindReactive() {
        // Create data source for table view
        let dataSource = RxTableViewSectionedReloadDataSource<ArticleSection>(
            configureCell: { (_, tableView, indexPath, item) in
                switch item {
                case .dataItem(let data):
                    let cell: ArticlesTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    cell.bind(data)
                    return cell
                case .skeletonItem:
                    let cell: ArticlesTableViewSkeletonCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    return cell
                }
            })

        // Bind section with data sourceamz
        presenter?.section
            .bind(to: articlesTableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        // Action when cell is selected
        articlesTableView.rx.itemSelected
            .subscribe(onNext: { [weak self] in
                guard let self = self,
                      let data = dataSource.sectionModels.first?.items[$0.row]
                else { return }

                switch data {
                case .dataItem(let data):
                    self.presenter?.navigateToWebView(from: self, url: data.url)
                default:
                    break
                }
            }).disposed(by: disposeBag)

        // Set delegate to remove header
        articlesTableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        // Action when refresh contol is pulled
        refreshControl.rx.controlEvent(.valueChanged)
            .subscribe(onNext: { [weak self] in
                self?.presenter?.reloadArticles()
            }).disposed(by: disposeBag)

        // Subscribe to data
        presenter?.section
            .subscribe(onNext: { [weak self] in
                guard let self = self, let items = $0.first?.items else { return }
                // Stop refresh control
                if self.refreshControl.isRefreshing == true {
                    self.refreshControl.endRefreshing()
                }

                // Hide or show tablel view and negative view based on items
                self.articlesTableView.isHidden = items.isEmpty
                self.negativeView.isHidden = !items.isEmpty

                // Set negative view if items is empty
                if items.isEmpty {
                    self.negativeView.setView(.empty(label: "Article not found"))
                }

                // Remove footer loading view
                if self.articlesTableView.tableFooterView != nil {
                    self.articlesTableView.tableFooterView = nil
                }
            }).disposed(by: disposeBag)

        // Subscribe to changes in search text field
        searchTextField.rx.text.orEmpty
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] in
                self?.presenter?.searchArticles($0)
            }).disposed(by: disposeBag)

        // Subscribe to error
        presenter?.error
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.articlesTableView.isHidden = $0 != nil
                self.negativeView.isHidden = $0 == nil
                self.searchTextField.isEnabled = $0 == nil

                if let errorMessage = $0?.message {
                    self.negativeView.setView(
                        .error(label: errorMessage, action: {
                            self.presenter?.reloadArticles()
                        }))
                }
            }).disposed(by: disposeBag)

        // Load more data
        articlesTableView.rx.willDisplayCell
            .subscribe(onNext: { [weak self] in
                self?.presenter?.loadMoreArticles(pos: $0.indexPath.row)
            }).disposed(by: disposeBag)
    }

    func showFooterLoading() {
        articlesTableView.tableFooterView = CustomLoadingPlaceholder()
    }
}

extension ArticlesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
}
