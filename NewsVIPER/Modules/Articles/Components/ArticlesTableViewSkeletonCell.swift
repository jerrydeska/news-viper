//
//  ArticlesTableViewSkeletonCell.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import SnapKit
import SkeletonView

class ArticlesTableViewSkeletonCell: UITableViewCell {
    private lazy var articleImageSkeleton = CustomSkeletonView()
    private lazy var titleSkeleton = CustomSkeletonView()
    private lazy var authorDateSkeleton = CustomSkeletonView()
    private lazy var descriptionSkeleton = CustomSkeletonView()

    private lazy var labelSkeletonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .leading
        return stackView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupUI()
        setupConstraint()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        [articleImageSkeleton,
         titleSkeleton,
         authorDateSkeleton,
         descriptionSkeleton
        ].forEach { $0.showAnimatedGradientSkeleton() }
    }
}

extension ArticlesTableViewSkeletonCell {
    private func setupUI() {
        labelSkeletonStackView.addMultipleArrangeSubviews(
            titleSkeleton, authorDateSkeleton, descriptionSkeleton
        )
        labelSkeletonStackView.setCustomSpacing(8, after: authorDateSkeleton)
        contentView.addMultipleSubviews(articleImageSkeleton, labelSkeletonStackView)

        selectionStyle = .none
        isUserInteractionEnabled = false
    }

    private func setupConstraint() {
        articleImageSkeleton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(16)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.equalToSuperview().inset(16)
            $0.height.equalTo(150)
        }

        labelSkeletonStackView.snp.makeConstraints {
            $0.top.equalTo(articleImageSkeleton.snp.bottom).offset(16)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.bottom.equalToSuperview().inset(16)
        }

        titleSkeleton.snp.makeConstraints {
            $0.height.equalTo(16).priority(.high)
            $0.width.equalToSuperview().multipliedBy(0.7)
        }

        authorDateSkeleton.snp.makeConstraints {
            $0.height.equalTo(12).priority(.high)
            $0.width.equalToSuperview().multipliedBy(0.4)
        }

        descriptionSkeleton.snp.makeConstraints {
            $0.height.equalTo(36).priority(.high)
            $0.width.equalToSuperview()
        }
    }
}
