//
//  ArticlesTableViewCell.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import SnapKit
import Kingfisher

class ArticlesTableViewCell: UITableViewCell {
    private let articleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Title Placeholder",
                textColor: .label,
                textFont: .systemFont(ofSize: 16, weight: .bold),
                alignment: .left
            )
        label.attributedText = attributedText
        label.numberOfLines = 0
        return label
    }()

    private let authorDateLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Author \u{2022} Date Placeholder",
                textColor: .label,
                textFont: .systemFont(ofSize: 12),
                alignment: .left
            )
        label.attributedText = attributedText
        label.numberOfLines = 0
        return label
    }()

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Description Placeholder",
                textColor: .secondaryLabel,
                textFont: .systemFont(ofSize: 12),
                alignment: .left
            )
        label.attributedText = attributedText
        label.numberOfLines = 0
        return label
    }()

    private let labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        return stackView
    }()

    private let imagePlacholder = CustomLoadingPlaceholder()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupUI()
        setupConstraint()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(_ data: ArticlesDataCell) {
        // Remove image in case of wrong placement
        articleImageView.image = nil

        // Get cached image
        let cachedImage = ImageCache.default.retrieveImageInMemoryCache(forKey: data.image)

        // Use cached image if exist, if not fetch from URL
        if let cachedImage = cachedImage {
            articleImageView.image = cachedImage
        } else {
            let imageURL = URL(string: data.image)
            articleImageView.kf.setImage(with: imageURL, placeholder: imagePlacholder)
        }

        titleLabel.attributedText = titleLabel.attributedText?.changeText(data.title)
        authorDateLabel.attributedText = authorDateLabel.attributedText?.changeText(data.authorDate)
        
        let descriptionString = data.description.convertHTMLToString() ?? ""
        descriptionLabel.attributedText = descriptionLabel.attributedText?.changeText(descriptionString)
    }
}

extension ArticlesTableViewCell {
    private func setupUI() {
        labelStackView.addMultipleArrangeSubviews(titleLabel, authorDateLabel, descriptionLabel)
        labelStackView.setCustomSpacing(8, after: authorDateLabel)
        contentView.addMultipleSubviews(articleImageView, labelStackView)

        selectionStyle = .none
    }

    private func setupConstraint() {
        articleImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(16)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.equalToSuperview().inset(16)
            $0.height.equalTo(150)
        }

        labelStackView.snp.makeConstraints {
            $0.top.equalTo(articleImageView.snp.bottom).offset(16)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.bottom.equalToSuperview().inset(16)
        }
    }
}
