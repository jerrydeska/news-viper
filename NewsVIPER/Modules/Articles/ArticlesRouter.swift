//
//  ArticlesRouter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit

class ArticlesRouter: ArticlesRouterProtocol {
    static func createModule(source: String, sourceID: String) -> UIViewController {
        let presenter = ArticlesPresenter()
        let viewController = ArticlesViewController(source: source)
        viewController.presenter = presenter
        viewController.presenter?.interactor = ArticlesInteractor(
            request: .init(sources: sourceID)
        )
        viewController.presenter?.view = viewController
        viewController.presenter?.router = ArticlesRouter()
        viewController.presenter?.interactor?.presenter = presenter

        return viewController
    }

    func navigateToWebView(from view: ArticlesViewProtocol, url: URL) {
        let nextViewController = CustomWebViewController(url: url)
        let currentViewController = view as! ArticlesViewController
        currentViewController.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
