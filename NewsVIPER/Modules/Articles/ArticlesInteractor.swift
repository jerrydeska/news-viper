//
//  ArticlesInteractor.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import RxSwift
import RxCocoa

class ArticlesInteractor: ArticlesInteractorProtocol {
    weak var presenter: ArticlesPresenterProtocol?
    var request: ArticlesRequest

    private let apiRepo: APIRepo
    private let disposeBag = DisposeBag()
    private var tableItem = [ArticleItem]()

    init(
        request: ArticlesRequest,
        apiRepo: APIRepo = APIRepoImplementation()
    ) {
        self.request = request
        self.apiRepo = apiRepo
    }

    func getArticles(_ query: String?, previousItem: [ArticleItem]) {
        self.presenter?.error.accept(nil)
        
        if let query = query {
            request.q = query.isEmpty ? nil : query
        }

        apiRepo.getArticles(param: request)
            .subscribe(onNext: { [weak self] in
                if let data = $0.articles,
                   let totalResults = $0.totalResults,
                   let self = self {
                    // Send total result
                    self.presenter?.totalResults = totalResults
                    // Map response to be of type [ArticleItem]
                    let newTableItem = data.map { ArticleItem.dataItem($0.mappedResponse()) }
                    // Add new result and previous data together
                    self.tableItem = previousItem + newTableItem
                    // Send the data to presenter
                    self.presenter?.section.accept([.init(model: "", items: self.tableItem)])
                }
            }, onError: { [weak self] in
                self?.presenter?.section.accept([])
                self?.presenter?.error.accept($0 as? CustomError)
            }).disposed(by: disposeBag)
    }
}
