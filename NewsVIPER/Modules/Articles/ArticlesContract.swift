//
//  ArticlesContract.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import RxRelay

protocol ArticlesViewProtocol: AnyObject {
    func setupUI()
    func setupConstraint()
    func bindReactive()
    func showFooterLoading()
}

protocol ArticlesInteractorProtocol {
    var presenter: ArticlesPresenterProtocol? { get set }
    var request: ArticlesRequest { get set }

    func getArticles(_ query: String?, previousItem: [ArticleItem])
}

protocol ArticlesPresenterProtocol: AnyObject {
    var view: ArticlesViewProtocol? { get set }
    var interactor: ArticlesInteractorProtocol? { get set }
    var router: ArticlesRouterProtocol? { get set }

    var section: BehaviorRelay<[ArticleSection]> { get set }
    var error: BehaviorRelay<CustomError?> { get set }
    var totalResults: Int { get set }

    func viewDidLoad()
    func searchArticles(_ query: String?)
    func reloadArticles()
    func loadMoreArticles(pos: Int)
    func navigateToWebView(from view: ArticlesViewProtocol, url: URL)
}

protocol ArticlesRouterProtocol {
    static func createModule(source: String, sourceID: String) -> UIViewController
    func navigateToWebView(from view: ArticlesViewProtocol, url: URL)
}
