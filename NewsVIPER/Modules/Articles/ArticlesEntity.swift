//
//  ArticlesEntity.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import RxDataSources

// Map response to ArticlesDataCell
extension ArticlesData {
    func mappedResponse() -> ArticlesDataCell {
        let title = self.title?.replacingOccurrences(of: "<[^>]+>", with: "") ?? "No Title"

        let author = self.author ?? "No Author"
        let dateUnformatted = self.publishedAt ?? ""
        // Format the date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = .current
        let dateFormatted = dateFormatter.date(from: dateUnformatted)
        // Change the date format for ui display
        dateFormatter.dateFormat = "dd MMMM yyyy"
        // Set date as `No Date` if date is nil
        var date = "No Date"
        if let dateFormatted = dateFormatted {
            date = dateFormatter.string(from: dateFormatted)
        }
        // Combine author and date
        let authorDate = "\(author) \u{2022} \(date)"

        let description = self.description?.replacingOccurrences(of: "<[^>]+>", with: "") ?? ""
        let image = self.urlToImage ?? "https://via.placeholder.com/300x150.png?text=No+Image"
        let url = URL(string: self.url ?? "") ?? URL(string: "https://www.google.com")!

        return .init(
            title: title,
            authorDate: authorDate,
            description: description,
            image: image,
            url: url
        )
    }
}

struct ArticlesDataCell: Equatable {
    let title: String
    let authorDate: String
    let description: String
    let image: String
    let url: URL
}

// Section for RxDataSource
enum ArticleItem: Equatable {
    case skeletonItem
    case dataItem(ArticlesDataCell)

    static func == (lhs: ArticleItem, rhs: ArticleItem) -> Bool {
        switch (lhs, rhs) {
        case (.skeletonItem, .skeletonItem):
            return true
        case (dataItem(let lData), dataItem(let rData)):
            return lData == rData
        default:
            return false
        }
    }
}

typealias ArticleSection = SectionModel<String, ArticleItem>
