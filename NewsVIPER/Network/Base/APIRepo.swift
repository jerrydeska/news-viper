//
//  APIRepo.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import RxSwift
import RxCocoa
import Alamofire

protocol APIRepo {
    func getSources(param: SourcesRequest) -> Observable<SourcesResponse>
    func getArticles(param: ArticlesRequest) -> Observable<ArticlesResponse>
}

class APIRepoImplementation: APIRepo {
    func getSources(param: SourcesRequest) -> Observable<SourcesResponse> {
        return APIClient.request(
            with: APIRouter.getSources(param: param.asDictionary())
        )
    }

    func getArticles(param: ArticlesRequest) -> Observable<ArticlesResponse> {
        return APIClient.request(
            with: APIRouter.getArticles(param: param.asDictionary())
        )
    }
}
