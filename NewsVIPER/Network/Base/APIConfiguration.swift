//
//  APIConfiguration.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import Alamofire

protocol APIConfiguration: URLRequestConvertible {
    var baseURL: String { get }
    var headers: [String: String]? { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var parameterEncoding: ParameterEncoding { get }
}

extension APIConfiguration {
    var baseURL: String {
        return "https://newsapi.org/v2/"
    }

    var headers: [String: String]? {
        return [
            "X-Api-Key": "96098b36fb6943f49cc1dc5c4d7fbb33",
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
    }

    var parameterEncoding: ParameterEncoding {
        return URLEncoding(destination: .queryString)
    }

    var method: HTTPMethod {
        return .get
    }

    func asURLRequest() throws -> URLRequest {
        let url = try baseURL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        headers?.forEach({ (key, value) in
            urlRequest.setValue(value, forHTTPHeaderField: key)
        })

        return try parameterEncoding.encode(urlRequest, with: parameters)
    }
}
