//
//  APIClient.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import Alamofire
import RxSwift
import RxCocoa

struct CustomError: Error, Decodable, Equatable {
    let status: String?
    let code: String?
    let message: String?
}

class APIClient {
    static func request<T: Codable>(
        with urlConvertible: URLRequestConvertible
    ) -> Observable<T> {
        return Observable<T>.create { observer in
            let request = AF.request(urlConvertible).validate().responseDecodable { (response: DataResponse<T, AFError>) in
                switch response.result {
                case .success(let value):
                    observer.onNext(value)
                    observer.onCompleted()
                case .failure:
                    if let data = response.data,
                       let customError = try? JSONDecoder().decode(CustomError.self, from: data) {
                        observer.onError(customError)
                    } else {
                        observer.onError(
                            CustomError.init(
                                status: nil,
                                code: nil,
                                message: "Something went wrong"
                            )
                        )
                    }
                }
            }

            return Disposables.create {
                request.cancel()
            }
        }
    }
}
