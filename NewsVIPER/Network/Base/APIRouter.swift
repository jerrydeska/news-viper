//
//  APIRouter.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

import Alamofire

enum APIRouter: APIConfiguration {
    case getSources(param: Parameters)
    case getArticles(param: Parameters)

    var path: String {
        switch self {
        case .getSources:
            return "top-headlines/sources"
        case .getArticles:
            return "top-headlines"
        }
    }

    var parameters: Parameters? {
        switch self {
        case .getSources(let param),
                .getArticles(let param):
            return param
        }
    }
}
