//
//  ArticlesResponse.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

struct ArticlesResponse: Codable {
    let status: String?
    let totalResults: Int?
    let articles: [ArticlesData]?
}

struct ArticlesData: Codable {
    let source: ArticleSource?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
}

struct ArticleSource: Codable {
    let id: String?
    let name: String?
}
