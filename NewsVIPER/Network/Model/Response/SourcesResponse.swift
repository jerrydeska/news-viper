//
//  SourcesResponse.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

struct SourcesResponse: Codable {
    let status: String?
    let sources: [SourceData]?
}

struct SourceData: Codable {
    let id: String?
    let name: String?
    let description: String?
    let url: String?
    let category: String?
    let language: String?
    let country: String?
}
