//
//  ArticlesRequest.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

struct ArticlesRequest: Encodable {
    let sources: String
    var q: String?
    let pageSize: Int
    var page: Int

    init(
        sources: String,
        q: String? = nil,
        pageSize: Int = 5,
        page: Int = 1
    ) {
        self.sources = sources
        self.q = q
        self.pageSize = pageSize
        self.page = page
    }

    mutating func loadNextPage() {
        self.page += 1
    }

    mutating func resetPage() {
        self.page = 1
        self.q = nil
    }
}
