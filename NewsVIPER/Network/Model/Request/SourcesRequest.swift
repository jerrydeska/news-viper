//
//  SourcesRequest.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 13/12/22.
//

struct SourcesRequest: Encodable {
    let category: String?
    let language: String?
    let country: String?
}
