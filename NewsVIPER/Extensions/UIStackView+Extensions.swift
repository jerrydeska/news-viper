//
//  UIStackView+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension UIStackView {
    func addMultipleArrangeSubviews(_ view: UIView...) {
        view.forEach {
            self.addArrangedSubview($0)
        }
    }
}
