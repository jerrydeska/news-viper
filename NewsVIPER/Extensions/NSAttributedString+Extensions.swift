//
//  NSAttributedString+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension NSAttributedString {
    func changeText(_ text: String) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(attributedString: self)
        attributedString.mutableString.setString(text)
        return attributedString
    }
}
