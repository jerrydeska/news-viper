//
//  UITableView+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension UITableView {
    func registerCells(_ cellClasses: UITableViewCell.Type...) {
        for cellClass in cellClasses {
            register(
                cellClass,
                forCellReuseIdentifier: String(describing: cellClass)
            )
        }
    }

    func dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(
            withIdentifier: String(describing: T.self),
            for: indexPath
        ) as? T else {
            fatalError("Failed to dequeue reusable cell with identifier: \(String(describing: T.self))")
        }

        return cell
    }
}
