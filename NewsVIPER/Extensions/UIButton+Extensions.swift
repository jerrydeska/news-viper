//
//  UIButton+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension UIButton {
    func setTitle(
        text: String
    ) {
        let attributedTitle = NSMutableAttributedString
            .setAttributedString(
                text: text,
                textColor: .systemBackground,
                textFont: .systemFont(ofSize: 16, weight: .bold),
                alignment: .left
            )

        self.setAttributedTitle(attributedTitle, for: .normal)
    }
}
