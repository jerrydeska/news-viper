//
//  UITextField.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension UITextField {
    func setPlaceholder(
        text: String
    ) {
        let attributedPlaceholder = NSMutableAttributedString
            .setAttributedString(
                text: text,
                textColor: .placeholderText,
                textFont: .systemFont(ofSize: 16),
                alignment: .left
            )
        
        self.attributedPlaceholder = attributedPlaceholder
    }
}
