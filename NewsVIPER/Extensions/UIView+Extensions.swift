//
//  UIView+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

typealias ActionTap = () -> Void

extension UIView {
    func addMultipleSubviews(_ view: UIView...) {
        view.forEach {
            self.addSubview($0)
        }
    }
}
