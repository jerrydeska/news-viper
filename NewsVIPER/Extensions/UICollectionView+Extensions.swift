//
//  UICollectionView+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension UICollectionView {
    func registerCells(_ cellClasses: UICollectionViewCell.Type...) {
        for cellClass in cellClasses {
            register(
                cellClass,
                forCellWithReuseIdentifier: String(describing: cellClass)
            )
        }
    }

    func dequeueReusableCell<T: UICollectionViewCell>(indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(
            withReuseIdentifier: String(describing: T.self),
            for: indexPath
        ) as? T else {
            fatalError("Failed to dequeue reusable cell with identifier: \(String(describing: T.self))")
        }

        return cell
    }
}
