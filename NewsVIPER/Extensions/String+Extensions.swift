//
//  String+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Perdana on 22/02/24.
//

import Foundation

extension String {
    // Convert HTML to NSAttributedString and take the string value
    func convertHTMLToString() -> String? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }
        
        do {
            let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
                .documentType: NSAttributedString.DocumentType.html,
                .characterEncoding: String.Encoding.utf8.rawValue
            ]
            
            let attributedString = try NSAttributedString(
                data: data,
                options: options,
                documentAttributes: nil
            )
            
            return attributedString.string
        } catch {
            print("Error converting HTML to String: \(error.localizedDescription)")
            return nil
        }
    }
}
