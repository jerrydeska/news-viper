//
//  NSMutableAttributedString+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension NSMutableAttributedString {
    static func setAttributedString(
        text: String,
        textColor: UIColor,
        textFont: UIFont,
        alignment: NSTextAlignment
    ) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment

        let textRange = NSRange(location: 0, length: attributedString.length)

        attributedString.addAttributes([
            NSAttributedString.Key.paragraphStyle: paragraphStyle,
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor
        ], range: textRange)

        return attributedString
    }
}
