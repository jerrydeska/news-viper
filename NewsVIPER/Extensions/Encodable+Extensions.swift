//
//  Encodable+Extensions.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit

extension Encodable {
    func asDictionary() -> [String: Any] {
        guard let data = try? JSONEncoder().encode(self),
              let dictionary = try? JSONSerialization.jsonObject(
                with: data,
                options: .fragmentsAllowed
              ) as? [String: Any] else {
            return [:]
        }
        return dictionary
    }
}
