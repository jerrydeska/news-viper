//
//  CustomNegativeView.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit
import RxSwift
import RxGesture
import SnapKit

enum CustomNegativeType {
    case error(label: String, action: ActionTap?)
    case empty(label: String)

    var text: String {
        switch self {
        case .error(let label, _),
                .empty(let label):
            return label
        }
    }

    var image: String {
        switch self {
        case .error:
            return "exclamationmark.triangle"
        case .empty:
            return "questionmark.folder"
        }
    }

    var hasButton: Bool {
        switch self {
        case .error:
            return true
        case .empty:
            return false
        }
    }

    var action: ActionTap? {
        switch self {
        case .error(_, let action):
            return action
        case .empty:
            return nil
        }
    }
}

class CustomNegativeView: UIView {
    private var negativeAction: ActionTap?
    private let disposeBag = DisposeBag()

    private let negativeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .label
        return imageView
    }()

    private let negativeLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString
            .setAttributedString(
                text: "Placeholder",
                textColor: .label,
                textFont: .systemFont(ofSize: 16),
                alignment: .center
            )
        label.numberOfLines = 0
        label.attributedText = attributedText
        return label
    }()

    private let negativeButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemRed
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.setTitle(text: "Try Again")
        return button
    }()

    private let informationStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 8
        return stackView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        setupConstraint()
        setupAction()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setView(_ type: CustomNegativeType) {
        negativeLabel.attributedText = negativeLabel.attributedText?.changeText(type.text)
        negativeImageView.image = UIImage(systemName: type.image)
        negativeButton.isHidden = !type.hasButton
        negativeAction = type.action
    }
}

extension CustomNegativeView {
    private func setupUI() {
        informationStack.addMultipleArrangeSubviews(negativeImageView, negativeLabel)
        addMultipleSubviews(informationStack, negativeButton)
    }

    private func setupConstraint() {
        informationStack.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }

        negativeImageView.snp.makeConstraints {
            $0.size.equalTo(80)
        }

        negativeButton.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview()
            $0.height.equalTo(48)
        }
    }

    private func setupAction() {
        negativeButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.negativeAction?()
            }).disposed(by: disposeBag)
    }
}
