//
//  CustomSkeletonView.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 14/12/22.
//

import UIKit
import SkeletonView

class CustomSkeletonView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isSkeletonable = true
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
