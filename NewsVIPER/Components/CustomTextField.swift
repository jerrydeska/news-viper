//
//  CustomTextField.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit
import RxSwift
import SnapKit

class CustomTextField: UITextField {
    private let padding = UIEdgeInsets(top: 8, left: 32, bottom: 8, right: 32)
    private let disposeBag = DisposeBag()

    override init(frame: CGRect) {
        super.init(frame: frame)

        layer.cornerRadius = 4
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
        clipsToBounds = true
        backgroundColor = .systemBackground
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

extension CustomTextField {
    enum TextFieldImageType {
        case left
        case right
    }

    func setSystemImage(
        image: String,
        type: TextFieldImageType
    ) {
        let imageView: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(systemName: image)?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = .lightGray
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()

        let container = UIView()

        container.addSubview(imageView)

        container.snp.makeConstraints {
            $0.size.equalTo(32)
        }

        imageView.snp.makeConstraints {
            $0.size.equalTo(16)
            $0.center.equalToSuperview()
        }

        switch type {
        case .left:
            leftView = container
            leftViewMode = .always
        case .right:
            rightView = container
            rightViewMode = .always
        }
    }
}
