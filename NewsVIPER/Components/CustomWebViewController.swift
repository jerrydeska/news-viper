//
//  CustomWebViewController.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 15/12/22.
//

import UIKit
import SnapKit
import WebKit

class CustomWebViewController: CustomViewController {
    private let url: URL

    private let webView: WKWebView = {
        let webView = WKWebView()
        webView.allowsBackForwardNavigationGestures = true
        return webView
    }()

    private let loadingPlaceholder = CustomLoadingPlaceholder()
    
    private let loadingContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = .secondarySystemBackground
        return view
    }()

    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupConstraint()
    }
}

extension CustomWebViewController {
    private func setupUI() {
        loadingContainer.addSubview(loadingPlaceholder)
        view.addMultipleSubviews(webView, loadingContainer)
        webView.load(URLRequest(url: url))

        webView.navigationDelegate = self
    }

    private func setupConstraint() {
        webView.snp.makeConstraints {
            $0.top.bottom.equalTo(view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
        }

        loadingContainer.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        loadingPlaceholder.snp.makeConstraints {
            $0.size.equalTo(50)
            $0.top.leading.equalToSuperview().offset(4)
            $0.trailing.bottom.equalToSuperview().inset(4)
        }
    }
}

extension CustomWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        loadingContainer.removeFromSuperview()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loadingContainer.removeFromSuperview()
    }
}
