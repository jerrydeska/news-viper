//
//  CustomLoadingPlaceholder.swift
//  NewsVIPER
//
//  Created by Jerry Deska on 12/12/22.
//

import UIKit
import SnapKit
import Kingfisher

class CustomLoadingPlaceholder: UIView, Placeholder {
    private let loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.hidesWhenStopped = true
        indicator.style = .medium
        indicator.startAnimating()
        return indicator
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        setupConstraint()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomLoadingPlaceholder {
    private func setupUI() {
        addSubview(loadingIndicator)
    }

    private func setupConstraint() {
        loadingIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}
